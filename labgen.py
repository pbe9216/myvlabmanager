import sys 
import os
import argparse
from pathlib import Path
from bs4 import BeautifulSoup
import yaml 
import netifaces
import ipaddress
import re
import getpass

# SYSTEM

class GlobalHelpers:

    # this function is used to find all bridge interfaces present in the system
    def listbr(self):
        listintf = []
        pattern_br = re.compile("^br[0-9]*$") 
        for intf in netifaces.interfaces():
            if pattern_br.match(intf):
                listintf.append(intf)

        return(listintf)

    # this function is used to find the next br number available 
    def nextbr(self):
        listbridge = self.listbr()

        # transform br in a number to find availabe ones
        listbrid = []
        for br in listbridge:
            listbrid.append(int(br.replace("br", "")))

        # find next available
        if listbrid:
            try:
                nextavailable = next(id for id in range(0,max(listbrid)) if id not in listbrid)
            except StopIteration:
                nextavailable = max(listbrid) + 1
        else:
            nextavailable = 0

        return("br"+ str(nextavailable))

    # this function is used to provision IP and write them in a host file
    def findip(self, hostname, subnet, project_name):
        networkobj = ipaddress.ip_network(subnet)
        pathtohf = project_name + "/hosts"
        
        try:
            hostfile = open(pathtohf,"a+")
        except:
            hostfile = open(pathtohf,"w+")

        ite = 0
        for host_ip in networkobj.hosts():
            hostfile.seek(0)
            if ite == 0:
                ite = ite + 1
            else:
                if str(host_ip) not in hostfile.read():
                    hostfile.write(str(host_ip) + "  " + hostname + "\n")
                    break

        hostfile.close()

        return str(host_ip)

    # this function is used to check if a key exists in a dict
    def check_key(self, dict, key):
        if key in dict.keys():
            present = True
        else:
            present = False

        return present

    # this function is used to determine the vagrant image name based on the type field 
    def imagelookup(self, description):

        extracted_description = description.split("=")
        
        try:
            with open("types.yml", 'r') as type_file:
                types = yaml.load(type_file, Loader=yaml.FullLoader)
        except:
            print('[ERROR] Something went wrong trying to read types.yml')
            sys.exit(0)

        vagrant_img_config = {}

        for img in types["images"]: 
            if img["shortname"] == extracted_description[1].strip():
                vagrant_img_config["img"] = img["vagrant_img"]
                if self.check_key(img, "memory") is True:
                    vagrant_img_config["memory"] = img["memory"]
                else:
                    vagrant_img_config["memory"] = "default"
                
                if self.check_key(img, "cpu") is True:
                    vagrant_img_config["cpu"] = img["cpu"]
                else: 
                    vagrant_img_config["cpu"] = "default"
                
                if img["provider"] == "libvirt":
                    vagrant_img_config["provider"] = "libvirt"
                elif img["provider"] == "virtualbox":
                    vagrant_img_config["provider"] = "virtualbox"
                else:
                    vagrant_img_config["provider"] = "libvirt"
                
                if self.check_key(img, "serial") is True:
                    if img["serial"] is True:
                        vagrant_img_config["serial"] = "yes"
                    else:
                        vagrant_img_config["serial"] = "no"
                else:
                    vagrant_img_config["serial"] = "no"

        
        if bool(vagrant_img_config) is False:
            print('[ERROR] The vagrant img ' + extracted_description[1] + ' is not supported or wrongly declared in types.yml')
            print('[ERROR] Please check types.yml file.')
            sys.exit(0)

        return(vagrant_img_config)

# GRAPML

class GraphmlHelpers:

    # this function is used to parse the graphml file and extract important data
    def parsegraphml(topology_file, admin_subnet, project_name, verbose):

        # verbose handling 
        verboseprint = print if verbose else lambda *a, **k: None

        try: 
            with open(topology_file) as file:
                soup = BeautifulSoup(file, "xml")
                nodes = soup.findAll("node")
                edges = soup.findAll("edge")
        except Exception as e:
                print("[ERROR] Failed to open the graphml file : " + str(e))
                sys.exit(0)
        
        file.close()
        
        # Nodes and connections list for later use for YAML file construction.
        nodeslist = []
        connectionslist = []

        for node in nodes:
            nodeid = node['id']
            nodetext = node.find("y:NodeLabel").text.replace(".", "").replace("-", "").replace("_","")
            nodetype = node.find("data", {"key" : "d5"}).text
            if nodetype == "":
                print("[ERROR] Failed to parse type= at node " + nodetext + " or type is missing.")
                sys.exit(0)
            gh = GlobalHelpers()
            nodeimg = gh.imagelookup(nodetype)
            nodeadmip = gh.findip(nodetext, admin_subnet, project_name)
            verboseprint("[INFO] Discovering node: " + nodeid + " with name: " + nodetext + " type: " + nodetype + " img: " + nodeimg["img"] + " admin ip: " + nodeadmip)
            nodeslist.append({"id" : nodeid, "name" : nodetext, "img" : nodeimg["img"], "ip" : nodeadmip, "provider" : nodeimg["provider"], "memory" : nodeimg["memory"], "cpu" : nodeimg["cpu"], "serial" : nodeimg["serial"]})

        for edge in edges:
            edgeid = edge['id']
            edgesrc = edge['source']
            edgedst = edge['target']
            verboseprint("[INFO] Discovering link: " + edgeid + " from " + edgesrc + " to " + edgedst)
            connectionslist.append({"id" : edgeid, "src" : edgesrc, "dst" : edgedst})

        verboseprint("[INFO] Validating connection scheme")
        connectionslistname = []
        mappingconnstodevice = []
        for a in connectionslist:
            for b in nodeslist:
                if b["id"] == a["src"]:
                    sourcecheck = b["name"]
                    mappingconnstodevice.append({"id" : a["id"], "device" : sourcecheck})
                elif b["id"] == a["dst"]:
                    destcheck = b["name"]
                    mappingconnstodevice.append({"id" : a["id"], "device" : destcheck})
            verboseprint("[INFO] Link from " + sourcecheck + " to " + destcheck)
            connectionslistname.append({"id" : a["id"], "name" : sourcecheck + "<->" + destcheck})

        return nodeslist, connectionslist, connectionslistname, mappingconnstodevice

# Create VAGRANT files

class VagrantHelpers:

    # this function is used to create the vagrantfile for the project 
    def createfile(nodeslist, connectionslist, connectionslistname, mappingconnstodevice, project_name, graphml_file, admin_subnet, verbose, webfolder):
        
        # verbose handling 
        verboseprint = print if verbose else lambda *a, **k: None

        # get sudo password
        sudopass = getpass.getpass(prompt='A privilege elevation is needed to continue, please type sudo password (if None, leave empty): ')

        # create / open labguide file
        try:
            labguide = open(project_name + "/labguide", "a+")
        except:
            labguide = open(project_name + "/labguide", "w+")

        # html output
        ipws = "localhost"
        if webfolder is not None:
            ipws = input ("On which IP does your webserver listen? ")
            try:
                os.system("echo " + sudopass + ' | sudo -S bash -c "touch '+ webfolder + project_name + '.html"')
                os.system("echo " + sudopass + ' | sudo -S bash -c "chmod 777 '+ webfolder + project_name + '.html"')
                htmlout = open(webfolder + project_name + ".html", "w+")
            except:
                htmlout = open(webfolder + project_name + ".html", "w+")

        htmlwrite = lambda text: htmlout.write(text) if webfolder is not None else False

        # create bridges
        try:
            bridgelist = open(project_name + "/interfaces", "a+")
        except:
            bridgelist = open(project_name + "/interfaces", "w+")
       
        # kernel features
        os.system("uname -a")

        kernel_features = None
        while kernel_features is None:
            custom_kernel = input ("Are you running a custom kernel ? (y/n)")
            if custom_kernel == "y" or custom_kernel == "Y":
                kernel_features = True
            elif custom_kernel == "n" or custom_kernel == "N":
                kernel_features = False

        if kernel_features is True:
            restricted_fwd = 0
            restricted_fwd = input ("Which protocol would you like to enable working in the lab ? (bitmask value, leave to 0 if you do not know): ")
            if restricted_fwd is "":
                restricted_fwd = 0

        htmlwrite("<html>")
        htmlwrite("<p>")
        htmlwrite("<b>BRIDGES</b><br />")
        labguide.write("[BRIDGES]\n")

        for conns in connectionslistname:
            bn = GlobalHelpers()
            brname = bn.nextbr() 
            verboseprint("[INFO] Creating: " + brname + " for project " + project_name + " where connection id is " + conns["id"] + " and connection description is " + conns["name"])
            labguide.write("Bridge: " + brname + " - Connection: " + conns["name"] + " ID: " + conns["id"] + "\n")
            htmlwrite("Bridge: " + brname + " - Connection: " + conns["name"] + " ID: " + conns["id"] + "<br />")
            conns["br"] = brname
            bridgelist.write(brname + "\n")

            # push os config
            try: 
                os.system("echo " + sudopass + " | sudo -S ip link add name " + brname + " type bridge")
                os.system("echo " + sudopass + " | sudo -S ip link set " + brname + " up ")
                verboseprint("[INFO] Bridge interfaces " + brname + " created")
                os.system("echo " + sudopass + ' | sudo -S bash -c "echo 0 > /sys/devices/virtual/net/' + brname + '/bridge/multicast_snooping"')
                if kernel_features is True:
                    os.system("echo " + sudopass + ' | sudo -S bash -c "echo ' + restricted_fwd + ' > /sys/devices/virtual/net/' + brname + '/bridge/group_fwd_mask"')
            except:
                print("[ERROR] Failed. Requires privilege elevation.")
                sys.exit(0)
        
        bridgelist.close()

        # assemble file
        pathtovf = project_name + "/Vagrantfile"
        vagrantfile = open(pathtovf, "w+")
        vagrantfile.write('VAGRANTFILE_API_VERSION = "2"\n')
        vagrantfile.write('\n')
        vagrantfile.write('Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|\n')
        vagrantfile.write('\n')

        labguide.write("\n[BOXES]\n")
        htmlwrite("<b>BOXES</b><br />")

        for nd in nodeslist:
            if nd["provider"] == "libvirt":
                vagrantfile.write('  # VM: ' + nd['name'] + " - id : " + nd["id"] + "\n")
                vagrantfile.write("  config.vm.define :" + nd["name"] + " do |" + nd["name"] + "|\n")
                vagrantfile.write("    " + nd["name"] + '.vm.box = "' + nd["img"] + '"\n')
                vagrantfile.write("    " + nd["name"] + '.ssh.insert_key = false\n')
                vagrantfile.write("    " + nd["name"] + '.vm.synced_folder ".", "/vagrant", disabled: true\n')
                vagrantfile.write("    " + nd["name"] + '.vm.boot_timeout = 3600\n')     
                vagrantfile.write("    " + nd["name"] + '.vm.network "private_network", ip: "'+ nd["ip"] +'"\n')
                labguide.write("Box name: " + nd["name"] + " - Image: " + nd["img"] + " - Provider: " + nd["provider"] + " - Admin IP: " + nd["ip"] + "\n")
                for conns2 in mappingconnstodevice:
                    if nd["name"] == conns2["device"]:
                        for conns3 in connectionslistname:
                            if conns2["id"] == conns3["id"]:
                                vagrantfile.write("    " + nd["name"] + '.vm.network "public_network", type: "bridge", dev: "' + conns3["br"] + '", mode: "bridge", auto_config: false\n')
                                labguide.write("  Connected to the bridge: " + conns3["br"] + "\n")

                vagrantfile.write("    " + nd["name"] + '.vm.provider :libvirt do |v| \n')
                vagrantfile.write("    end\n")
                vagrantfile.write("  end\n")
            elif nd["provider"] == "virtualbox":
                vagrantfile.write('  # VM: ' + nd['name'] + " - id : " + nd["id"] + "\n")
                vagrantfile.write("  config.vm.define :" + nd["name"] + " do |" + nd["name"] + "|\n")
                vagrantfile.write("    " + nd["name"] + '.vm.box = "' + nd["img"] + '"\n')
                vagrantfile.write("    " + nd["name"] + '.vm.boot_timeout = 3600\n')   
                vagrantfile.write("    " + nd["name"] + '.vm.network "private_network", ip: "'+ nd["ip"] +'"\n')
                vagrantfile.write("    " + nd["name"] + '.ssh.insert_key = false\n')
                vagrantfile.write("    " + nd["name"] + '.vm.synced_folder ".", "/vagrant", disabled: true\n')
                labguide.write("Box name: " + nd["name"] + " - Image: " + nd["img"] + " - Provider: " + nd["provider"] + " - Admin IP: " + nd["ip"] + "\n")
                htmlwrite("Box name: " + nd["name"] + " - Image: " + nd["img"] + " - Provider: " + nd["provider"] + " - Admin IP: " + nd["ip"] + "<br />")
                vboxnicid = 1
                for conns2 in mappingconnstodevice:
                    if nd["name"] == conns2["device"]:
                        for conns3 in connectionslistname:
                            if conns2["id"] == conns3["id"]:
                                vagrantfile.write("    " + nd["name"] + '.vm.network "public_network", bridge: "' + conns3["br"] + '", auto_config: false\n')
                                labguide.write("  Connected to the bridge: " + conns3["br"] + "\n")
                                htmlwrite("  Connected to the bridge: " + conns3["br"] + "<br />")
                                vboxnicid = vboxnicid + 1
                
                # VM config
                vagrantfile.write("    " + nd["name"] + '.vm.provider :virtualbox do |v| \n')
                if str(nd["memory"]) != "default":
                    vagrantfile.write("          v.memory = " + str(nd["memory"]) + " \n")

                if str(nd["cpu"]) != "default":
                    vagrantfile.write("          v.cpus = " + str(nd["cpu"]) + " \n")	 
                    
                if nd["serial"] == "yes": 
                    vagrantfile.write('          v.customize ["modifyvm", :id, "--uart1", "0x3f8", "4"]\n')
                    #vagrantfile.write('          v.customize ["modifyvm", :id, "--uartmode1", "server", "/tmp/serial_' + nd["name"] + '"] \n')
                    serial_port_no = 10000 + int(nd["id"].replace("n", ""))
                    vagrantfile.write('          v.customize ["modifyvm", :id, "--uartmode1", "tcpserver", "' + str(serial_port_no) + '"] \n')
                    while vboxnicid != 0:
                        vagrantfile.write('          v.customize ["modifyvm", :id, "--nicpromisc' + str(vboxnicid) + '", "allow-all"]\n')
                        vboxnicid = vboxnicid - 1 
                    labguide.write("  Dedicated console available: tcpserver listening on port " + str(serial_port_no) + "\n")
                    htmlwrite("  Dedicated console available: <a href='telnet://" + ipws + ":" + str(serial_port_no) + "'>console</a><br />")

                vagrantfile.write("    end\n")
                vagrantfile.write("  end\n")
            else:
                print("[ERROR] Unknown provider listed in types.yml for" + nd["img"])
                sys.exit(0)

        vagrantfile.write("  end\n")
        vagrantfile.close()
        labguide.close()
        htmlwrite("</p>")
        htmlwrite("</html>")

        if webfolder is not None:
            htmlout.close()

        return None

    # this function is used to start your lab 
    def up(project_name, verbose):
        # verbose handling 
        verboseprint = print if verbose else lambda *a, **k: None
        verboseprint("[INFO] May the force be with you!")
        os.chdir(project_name)
        os.system("vagrant up")
        return None

    # this function is used to kill the lab
    def destroy_all(project_name, verbose):
        # verbose handling 
        verboseprint = print if verbose else lambda *a, **k: None
        verboseprint("[INFO] Shutting down the lab")
 
        try:
            os.chdir(project_name)
            os.system("vagrant destroy -f")
            alreadydeleted = False
        except:
            verboseprint("[INFO] Project folder already deleted.")
            alreadydeleted = True
        
        try:
            bridgelist = open("interfaces", "r")
        except:
            bridgelist = None

        if bridgelist is not None:
            sudopass = getpass.getpass(prompt='A privilege elevation is need, please type sudo password (if None, leave empty): ')
            for line in bridgelist:
                os.system("echo " + sudopass + " | sudo -S ip link delete " + line)
                verboseprint("[INFO] " + line.replace('\n',"") + " deleted.")

        if alreadydeleted is not True:
            os.chdir("../")
            os.system("rm -rf " + project_name + "/")

        return None

# MAIN functions

def main():
    # argparse 
    parser = argparse.ArgumentParser(description="LABGen: creates vagrant files to build your own lab based on GRAPHML input")
    parser.add_argument("-g", "--graphml_file", action="store", help="Your input graph file that describe your network.")
    parser.add_argument("-v", "--verbose", default=False, action="store_true", help="increase output verbosity")
    parser.add_argument("-p", "--project", action="store", help="define a project name")
    parser.add_argument("-c", "--create", action="store_true", help="prepare all the lab environment")
    parser.add_argument("-u", "--up", action="store_true", help="start your lab")
    parser.add_argument("-d", "--destroy", action="store_true", help="destroy the lab")
    parser.add_argument("--admin-subnet", action="store", help="define your own admin subnet")
    parser.add_argument("-w", "--webfolder", action="store", help="output an html file into the selected folder (requires a webserver and the absolute path)")
    
    # check if there is any args
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        sys.exit(0)
    else:
        args = parser.parse_args()
        # print(args)

    # Verbose function
    if args.verbose is True:
        verbose = True
        verboseprint = print if verbose else lambda *a, **k: None
        verboseprint("[INFO] Retreiving args")
    else:
        verbose = False
        
    # admin subnet 
    if args.admin_subnet is not None:
        admin_subnet = args.admin_subnet
    else:
        admin_subnet = "21.0.0.0/8"

    # start your lab arg
    if args.up is True:
        if args.project is not None:
            VagrantHelpers.up(args.project, verbose)
        else:
            print("[ERROR] Please provide your project folder / name.")
            sys.exit(0)

    # destroy lab 
    if args.destroy is True:
        if args.project is not None:
            VagrantHelpers.destroy_all(args.project, verbose)
        else:
            print("[ERROR] Please provide your project folder / name.")
            sys.exit(0) 

    if args.create is True:
        # create folder 
        if args.project is not None:
            project_name = args.project
        else:
            if args.graphml_file:
                project =  args.graphml_file.split(".")[0]
            else:
                print("[ERROR] Please provide your a graphml file.")
                sys.exit(0)

        if args.graphml_file:
            graphml_file = args.graphml_file
            currpath = os.getcwd()
            verboseprint("[INFO] The current working directory is " + currpath)
            try:
                os.mkdir(project_name)
            except:
                pass
        else:
            print("[ERROR] Please provide your a graphml file.") 
            sys.exit(0)

        if args.webfolder:
            webfolder = args.webfolder
            try: 
                os.listdir(args.webfolder)
            except:
                print("[ERROR] The folder provided does not exist or you don't have permission to access it.")
                sys.exit(0)
        else:
            webfolder = None

        print(graphml_file)
        # Parse graphml and extract data
        nodeslist, connectionslist, connectionslistname, mappingconnstodevice = GraphmlHelpers.parsegraphml(graphml_file, admin_subnet, project_name, verbose)

        # Generate vagrant files
        VagrantHelpers.createfile(nodeslist, connectionslist, connectionslistname, mappingconnstodevice, project_name, graphml_file, admin_subnet, verbose, webfolder)



# MAIN

if __name__ == '__main__':
    main()