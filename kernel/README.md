# Custom kernel list

Compiled kernels with permissive bridge module.

# Ubuntu 
Procedure: https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel

Precompiled kernel 
- vlab-kernel-5.0.0-31_amd64.tar.gz : tested on Ubuntu Bionic

```
uname -a
sudo apt build-dep linux-image-unsigned-5.0.0-31-generic
tar xvzf vlab-kernel-5.0.0-31_amd64.tar.gz
cd vlab-kernel-5.0.0-31_amd64/
sudo dpkg -i .deb
sudo reboot
uname -a
```
